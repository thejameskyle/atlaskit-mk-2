import * as React from 'react';
import { IconAndTitleLayout } from '../IconAndTitleLayout';
import Button from '@atlaskit/button';
import { truncateUrlForErrorView } from '../utils';
import { Frame } from '../Frame';
import { colors } from '@atlaskit/theme';
import { messages } from '../../messages';
import { FormattedMessage } from 'react-intl';

export interface InlineCardUnauthorizedViewProps {
  /** The url to display */
  url: string;
  /** The icon of the service (e.g. Dropbox/Asana/Google/etc) to display */
  icon?: string;
  /** The optional click handler */
  onClick?: React.EventHandler<React.MouseEvent | React.KeyboardEvent>;
  /** What to do when a user hit "Try another account" button */
  onAuthorise?: () => void;
  /** A flag that determines whether the card is selected in edit mode. */
  isSelected?: boolean;
}

export class InlineCardUnauthorizedView extends React.Component<
  InlineCardUnauthorizedViewProps
> {
  handleConnectAccount = (event: React.MouseEvent<HTMLElement>) => {
    const { onAuthorise } = this.props;
    event.preventDefault();
    event.stopPropagation();
    return onAuthorise!();
  };

  render() {
    const { url, icon, onClick, isSelected, onAuthorise } = this.props;
    return (
      <Frame onClick={onClick} isSelected={isSelected}>
        <IconAndTitleLayout
          icon={icon}
          title={truncateUrlForErrorView(url)}
          titleColor={colors.N500}
        />
        {!onAuthorise ? (
          ''
        ) : (
          <>
            {/* 
              NB: a non-breaking hyphen - hyphentation should be
              handled by the browser, not us.
            */}
            {` \u2011 `}
            <Button
              spacing="none"
              appearance="link"
              onClick={this.handleConnectAccount}
            >
              <FormattedMessage {...messages.connect_link_account} />
            </Button>
          </>
        )}
      </Frame>
    );
  }
}
